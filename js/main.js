angular.module('cardDemo1', ['ngMaterial','btford.socket-io','chart.js','ngWebSocket','ngAnimate','ui.router'])
  .controller('AppCtrl', function($scope,$http,socket,$interval,WebsocketFact,$mdSidenav) {

    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      }
    };
    $scope.Messages = WebsocketFact;
    $scope.options = {
      scales: {
        xAxes: [{
          display: false,
          ticks: {
            max: 125,
            min: -125,
            stepSize: 10
          }
        }],
        yAxes: [{
          display: false,
          ticks: {
            max: 125,
            min: -125,
            stepSize: 10
          }
        }]
      }
    };
    createChart();
    //$interval(createChart, 2000);
    function createChart () {
      $scope.series = [];
      $scope.data = [];
      for (var i = 0; i < 50; i++) {
        $scope.series.push(`Series ${i}`);
        $scope.data.push([{
          x: randomScalingFactor(),
          y: randomScalingFactor(),
          r: randomRadius()
        }]);
      }
    }
    function randomScalingFactor () {
      return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
    }
    function randomRadius () {
      return Math.abs(randomScalingFactor()) / 4;
    }
    $scope.imagePath = 'hello.png';
    $scope.capteurs=[];
    $scope.imageName=['Image0','Image1','Image2','Image3','Image4','Image5'];
    $scope.imageURL=['img/energy.svg','img/switchOn.svg','img/switchOff.svg','img/temp.svg','img/hum.svg','img/lux.svg'];
    $scope.lesDatas={};
    $http.get('http://192.168.1.31/relay').success(function(datas){
      $scope.lesDatas=datas;
      console.log(datas)
      socket.syncUpdates('esp', $scope.lesDatas,function(truc,bidule){
        $scope.lesDatas=truc;
        console.log(truc);
        createChart();
      });
    });
    $scope.capteurs.push(
      {
        'function':'sensor','name':'Température','valeur':'22','image':'img/temp.svg','code':'r1'
    },{
        'function':'sensor','name':'Humidité','valeur':'62','image':'img/hum.svg','code':'r2'
    },{
        'function':'sensor','name':'Luminosité','valeur':'','image':'img/lux.svg','code':'r3'
    },{
        'function':'sensor','name':'Energie','valeur':'80','image':'img/energy.svg','code':'r4'
    });

    $scope.allume=function(relay){
      $http.get('http://192.168.1.31/relay?'+relay+'=on').success(function(datas){
        $scope.lesDatas=datas;
        console.log(datas)
      });
    };
    $scope.eteinds=function(relay){
      $http.get('http://192.168.1.31/relay?'+relay+'=off').success(function(datas){
        $scope.lesDatas=datas;
        console.log(datas)
      });
    };

    $scope.allumeS=function(relay){
      WebsocketFact.send(relay+'on\n');
    };
    $scope.eteindsS=function(relay){
      WebsocketFact.send(relay+'off\n');
    };
  })
  .config(function($mdThemingProvider) {
    $mdThemingProvider.theme('dark-grey').backgroundPalette('grey').dark();
    $mdThemingProvider.theme('dark-orange').backgroundPalette('orange').dark();
    $mdThemingProvider.theme('dark-purple').backgroundPalette('deep-purple').dark();
    $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark();
  })
  .config(function ($stateProvider) {
    $stateProvider
      .state('homepage', {
        url: '/',
        templateUrl: 'views/homepage.html',
        controller: 'AppCtrl'
      })
      .state('esp', {
        url: '/esp',
        templateUrl: 'views/esp.html',
        controller: 'AppCtrl'
      })
      .state('test', {
        url: '/test',
        templateUrl: 'views/test.html',
        controller: 'AppCtrl'
      });
  });


var loadingImage = false;
function LoadImage(imageName,imageFile)
{
  if ((!document.images) || loadingImage) return;
  loadingImage = true;
  if (document.images[imageName].src.indexOf(imageFile)<0)
  {
    document.images[imageName].src = imageFile;
  }
  loadingImage = false;
}
LoadImage('image0','img/energy.svg');


