angular.module('cardDemo1')
.factory('socket', function(socketFactory) {
  var ioSocket = io('http://veinand.hd.free.fr:9000', {
    path: '/socket.io-client'
  });
  var socket = socketFactory({
    ioSocket: ioSocket
  });
  return {
    socket: socket,
    syncUpdates: function (modelName, array, cb) {
      cb = cb || angular.noop;

      socket.on(modelName + ':save', function (item) {
        console.log('socket:save');
        cb(item, array);
      });

      socket.on(modelName + ':remove', function (item) {
        console.log('socket:remove');
        cb(item, array);
      });
    },
    unsyncUpdates: function (modelName) {
      socket.removeAllListeners(modelName + ':save');
      socket.removeAllListeners(modelName + ':remove');
    }
  };
});
      


